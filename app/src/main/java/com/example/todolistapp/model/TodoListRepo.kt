package com.example.todolistapp.model

import com.example.todolistapp.model.dao.TaskListItem
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


class TodoListRepo @Inject constructor(val db: TaskItemDB){
    suspend fun getAllTaskItems() = withContext(Dispatchers.IO)
    { db.TaskItemDAO().getAllTasks() }

    suspend fun insertTaskItem(taskItem: TaskListItem) = withContext(Dispatchers.IO){
        db.TaskItemDAO().insertTask(taskItem)
    }
    suspend fun getTaskById(id_: Int) = withContext(Dispatchers.IO){
        db.TaskItemDAO().getTask(id_)
    }
}