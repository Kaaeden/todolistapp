package com.example.todolistapp.model

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.todolistapp.model.dao.TaskItemDAO
import com.example.todolistapp.model.dao.TaskListItem

@Database(entities = [TaskListItem::class], version = 1)
abstract class TaskItemDB : RoomDatabase() {
    abstract fun TaskItemDAO(): TaskItemDAO

}