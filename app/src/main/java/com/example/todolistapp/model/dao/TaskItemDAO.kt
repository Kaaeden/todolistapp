package com.example.todolistapp.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao
interface TaskItemDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTask(vararg tdItems: TaskListItem)
    @Query("SELECT * FROM tasklistdb")
    fun getAllTasks(): List<TaskListItem>
    @Query("SELECT * FROM tasklistdb WHERE uid = (:id)")
    fun getTask(id: Int): List<TaskListItem>
    @Update(entity = TaskListItem::class)
    fun updateCategory(task: TaskListItem)

}