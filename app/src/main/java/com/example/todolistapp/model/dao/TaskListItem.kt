package com.example.todolistapp.model.dao

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tasklistdb")
data class TaskListItem(

    @PrimaryKey(autoGenerate = true) val uid: Int = 0,
    @ColumnInfo(name = "task_name") val name: String?,
    @ColumnInfo(name = "task_desc") val desc: String?,
    @ColumnInfo(name = "status") val status: Boolean?,

)