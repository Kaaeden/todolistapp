package com.example.todolistapp.model.di

import androidx.room.Room
import com.example.createcontactsapp.TaskListApplication
import com.example.todolistapp.model.TaskItemDB
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object DataBaseModule {
    @Provides
    fun providesDB(): TaskItemDB {
        return Room.databaseBuilder(
            TaskListApplication.applicationContext(),
            TaskItemDB::class.java, "tasklistdb"
        ).build()
    }
}