package com.example.todolistapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.todolistapp.model.TodoListRepo
import com.example.todolistapp.model.dao.TaskListItem
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

@HiltViewModel
class TaskListViewModel @Inject constructor(private val repo: TodoListRepo): ViewModel() {

    private val _state = MutableStateFlow(TaskListState())
    val state get() = _state.asStateFlow()

    init {
        viewModelScope.launch{
          //  val taskItem = TaskListItem(0,"Complete App", "Finish the App", false)
          //  repo.insertTaskItem(taskItem)
            val records = repo.getAllTaskItems()
            _state.update{ it.copy(taskList = records)}
        }

    }

    fun updateTask(taskItem: TaskListItem)
    {
        viewModelScope.launch{
            repo.insertTaskItem(taskItem)
        }
    }

    fun getAllTask(){
    viewModelScope.launch {

        val records = repo.getAllTaskItems()
        _state.update{ it.copy(taskList = records)}
    }
    }

    fun insertNewTask(taskItem: TaskListItem) {
        viewModelScope.launch {
            repo.insertTaskItem(taskItem)
        }

    }


}