package com.example.todolistapp.viewmodel

import com.example.todolistapp.model.dao.TaskListItem

data class TaskListState(
    val isLoading: Boolean = false,
    val taskList: List<TaskListItem> = emptyList(), val onError: String = "")

