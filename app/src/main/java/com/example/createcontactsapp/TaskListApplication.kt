package com.example.createcontactsapp

import android.app.Application
import android.content.Context
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class TaskListApplication : Application() {

    init {
        instance = this
    }

    override fun onCreate() {
        super.onCreate()
        // initialize for any
        // Use ApplicationContext.
        // example: SharedPreferences etc...
    }

    companion object {
        private var instance: TaskListApplication? = null

        /**
         * Application context.
         *
         * @return
         */
        fun applicationContext(): Context {
            return instance!!.applicationContext
        }
    }

}