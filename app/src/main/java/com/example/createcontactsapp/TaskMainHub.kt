package com.example.createcontactsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Checkbox
import androidx.compose.material3.Divider
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.todolistapp.model.dao.TaskListItem
import com.example.todolistapp.viewmodel.TaskListViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class TaskMainHub  : Fragment() {

    private val taskListViewModel by activityViewModels<TaskListViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply{
            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent{

                val state = taskListViewModel.state.collectAsState()

                Column(modifier = Modifier
                    .background(Color.Gray)
                    .fillMaxWidth()
                    .fillMaxHeight()) {
                    LazyColumn(){
                        items(state.value.taskList){ tasks -> TaskListDisplayCard(taskData = tasks)}
                    }
                    Button(modifier = Modifier.fillMaxWidth().height(32.dp),onClick = { findNavController().navigate(R.id.taskCreator) }) { Text("Create New Task")}
                    }



            }

        }
    }

    @Composable
    fun TaskListDisplayCard(taskData: TaskListItem){
        val fullDisplay = remember { mutableStateOf(false) }
        Card(modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
            .height(100.dp)
            .clickable(onClick = {
                if (fullDisplay.value == false) {
                    fullDisplay.value = true
                    println(fullDisplay)
                } else {
                    fullDisplay.value = false
                }
            }), elevation = CardDefaults.cardElevation(4.dp), colors=CardDefaults.cardColors(containerColor = Color.LightGray)) {

            if (!fullDisplay.value) {
                Column(modifier= Modifier.fillMaxSize()) {
                    Text(
                        taskData.name.toString(),
                        fontSize = 15.sp,
                        fontWeight = FontWeight.Bold
                    )
                    Text(taskData.desc.toString(), fontSize = 15.sp, color = Color.Black)
                }
            }
            else {
                Column() {
                    Text(taskData.name.toString(), fontSize = 20.sp, fontWeight = FontWeight.Bold, 
                        modifier = Modifier.padding(10.dp))
                    Divider(color = Color.Gray, modifier = Modifier
                        .fillMaxWidth()
                        .height(1.dp)
                        .padding(5.dp))
                    Text(taskData.desc.toString(), fontSize = 15.sp, color = Color.Black)
                    Divider(color = Color.Blue, modifier = Modifier
                        .fillMaxWidth()
                        .height(1.dp)
                        .padding(5.dp))
                    CheckBoxItem(taskData)

                }
            }
        }
    }
    @Composable
    fun CheckBoxItem(taskData: TaskListItem) {
        val checkedState = remember { mutableStateOf(false) }
        Checkbox(
            checked = checkedState.value,
            onCheckedChange = { checkedState.value = it; taskListViewModel.updateTask(TaskListItem(taskData.uid, taskData.name, taskData.desc, it)) },
            modifier = Modifier.padding(2.dp)
        )
    }

}