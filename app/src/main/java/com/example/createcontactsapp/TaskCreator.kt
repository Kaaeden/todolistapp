package com.example.createcontactsapp

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.platform.ViewCompositionStrategy
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.example.todolistapp.model.dao.TaskListItem
import com.example.todolistapp.viewmodel.TaskListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TaskCreator : Fragment() {

    private val taskListViewModel by activityViewModels<TaskListViewModel>()

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return ComposeView(requireContext()).apply{

            setViewCompositionStrategy(ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed)
            setContent {
                var nameField by remember { mutableStateOf("") }
                var descriptionField by remember { mutableStateOf("") }
                Column(){
                TextField(value = nameField, onValueChange = { nameField = it }, label = { Text("Name") })
                TextField(value = descriptionField, onValueChange = { descriptionField = it }, label = { Text("Description") })
                Button(onClick={ taskListViewModel.insertNewTask(TaskListItem(taskListViewModel.state.value.taskList.size, nameField, descriptionField, false))
                findNavController().popBackStack()
                }){Text("Create Task")}
            }
            }
        }
    }

}